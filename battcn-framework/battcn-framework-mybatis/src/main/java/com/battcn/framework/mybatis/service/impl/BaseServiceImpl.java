package com.battcn.framework.mybatis.service.impl;

import com.battcn.framework.commons.entity.DataGrid;
import com.battcn.framework.mybatis.mapper.BaseMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.exceptions.TooManyResultsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * service通用基类
 *
 * @param <T>
 * @author Levin
 */
@Service
public abstract class BaseServiceImpl<T> implements com.battcn.framework.mybatis.service.BaseService<T> {

    @Autowired
    private BaseMapper<T> mapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean insert(T entity) {
        return mapper.insertSelective(entity) > 0;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean insertList(List<T> recordList) {
        return mapper.insertList(recordList) > 0;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean insertSelective(T entity) {
        return mapper.insertSelective(entity) > 0;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteById(Object key) {
        return mapper.deleteByPrimaryKey(key) > 0;
    }

    @Override
    public T selectById(Object key) {
        return mapper.selectByPrimaryKey(key);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateById(T entity) {
        return mapper.updateByPrimaryKey(entity) > 0;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateSelectiveById(T entity) {
        return mapper.updateByPrimaryKeySelective(entity) > 0;
    }

    @Override
    public List<T> listAll() {
        return this.mapper.selectAll();
    }

    @Override
    public List<T> select(T record) {
        return this.mapper.select(record);
    }

    @Override
    public T selectOne(T record) {
        return this.mapper.selectOne(record);
    }

    @Override
    public T findByExample(Example example) {
        final List<T> records = this.mapper.selectByExample(example);
        if (records == null) {
            return null;
        } else if (records.size() > 1) {
            throw new TooManyResultsException();
        }
        return records.get(0);
    }

    @Override
    public PageInfo<T> listForDataGrid(DataGrid grid) {
        return this.listForDataGrid(grid, null);
    }

    @Override
    public PageInfo<T> listForDataGrid(DataGrid grid, T entity) {
        PageHelper.startPage(grid.getPageNum(), grid.getPageSize()).setOrderBy(grid.getSort() + " " + grid.getOrder());
        return new PageInfo<T>(mapper.select(entity));
    }
}
