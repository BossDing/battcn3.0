package com.battcn.framework.security.model.token;

/**
 * @author Levin
 */
public interface Token {

    /**
     * 获取 Token
     *
     * @return Token
     */
    String getToken();
}
