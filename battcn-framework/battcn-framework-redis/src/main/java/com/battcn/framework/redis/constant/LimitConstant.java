package com.battcn.framework.redis.constant;

/**
 * @author Levin
 * @since 2018/2/5 0005
 */
public class LimitConstant {

    public static class KeyPrefix {
        public static final String IP = "ip:";
    }

    public static class Key {
        public static final String BLACKLIST = "ip:blacklist";
    }
}
