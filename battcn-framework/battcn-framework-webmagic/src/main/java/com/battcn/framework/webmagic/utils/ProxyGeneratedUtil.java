package com.battcn.framework.webmagic.utils;


/**
 * @author yanglei
 */
public class ProxyGeneratedUtil {

    public static String authHeader(String orderno, String secret, int timestamp) {
        //拼装签名字符串
        String planText = String.format("orderno=%s,secret=%s,timestamp=%d", orderno, secret, timestamp);

        //计算签名
        String sign = org.apache.commons.codec.digest.DigestUtils.md5Hex(planText).toUpperCase();

        //拼装请求头Proxy-Authorization的值
        return String.format("sign=%s&orderno=%s&timestamp=%d", sign, orderno, timestamp);
    }
}
