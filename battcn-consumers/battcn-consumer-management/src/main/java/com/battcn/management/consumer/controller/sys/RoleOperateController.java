package com.battcn.management.consumer.controller.sys;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.battcn.management.consumer.controller.BaseController;
import com.battcn.management.consumer.util.ApiResult;
import com.battcn.system.facade.RoleOperateService;
import com.battcn.system.pojo.po.RoleOperate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;


/**
 * @author Levin
 */
@Controller
@RequestMapping("/sys/role/operate/")
@ApiIgnore
public class RoleOperateController extends BaseController {

    @Reference(version = "1.0.0",
            application = "${dubbo.application.id}",
            url = "${dubbo.registry.address}")
    private RoleOperateService roleOperateService;

    @GetMapping("{roleId}")
    @ResponseBody
    public ApiResult<List<RoleOperate>> listOperate(@PathVariable Integer roleId) {
        return ApiResult.getSuccess(this.roleOperateService.listRoleOperateByRoleId(roleId));
    }

    @PostMapping("permissions")
    @ResponseBody
    public ApiResult<String> savePermissions(Integer[] operateId, Integer roleId) {
        logger.debug("[数据] - [{}] - [{}]", JSON.toJSONString(operateId), roleId);
        int rows = this.roleOperateService.batchInsertRoleOperate(operateId, roleId);
        return ApiResult.getResponse(rows > 0);
    }

}
